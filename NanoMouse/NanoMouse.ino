#include <Servo.h>

#define RIGHT 1
#define LEFT -1

byte power=250;

Servo leftServo;
Servo rightServo;
const byte ledPin = 13;
const byte buttonPin = 9;

void forward()
{
  leftServo.writeMicroseconds(1500-power);
  rightServo.writeMicroseconds(1500+power);
}
void stop(int time = 200)
{
  leftServo.writeMicroseconds(1500);
  rightServo.writeMicroseconds(1500);
}

void turn(int direction,int degrees)
{
  leftServo.writeMicroseconds(1500+power*direction);
  rightServo.writeMicroseconds(1500+power*direction);
  delay(degrees*5.9);
  stop();
}  

void forwardTime(unsigned int time)
{
  forward();
  delay(time);
  stop();
}  

void setup()
{
  pinMode(ledPin,OUTPUT);
  pinMode(buttonPin,INPUT_PULLUP);
  leftServo.attach(6);
  rightServo.attach(5);

  stop();

  while(digitalRead(buttonPin))
  {
  }

  for(int x=0;x<4;x++)
  {
    forwardTime(1500);
    turn(LEFT,90);
  }
}

void loop()
{
  digitalWrite(ledPin,HIGH);
}  



